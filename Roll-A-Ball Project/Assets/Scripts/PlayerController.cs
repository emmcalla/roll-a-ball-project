﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
//using System.Diagnostics;
//using System.Runtime.InteropServices;
//using System.Collections.Specialized;

public class PlayerController : MonoBehaviour
{
    private const bool V = true;
    public float speed = 0;
    public TextMeshProUGUI counttext;
    public GameObject winTextObject;
    public Vector3 jump;
    public float jumpForce = 10.0f;
    public bool isGrounded;

    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;

        SetCountText();
        winTextObject.SetActive(false);
    }

    private void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        counttext.text = "Count: " + count.ToString();
        if(count >= 11)
        {
            winTextObject.SetActive(true);
        }
    }

    private void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;

            SetCountText();
        }
        
    }

    void OnCollisionStay()
    {
        isGrounded = V;
    }

    void OnJump(InputValue MovementValue)
    {
        Debug.Log("working");
        rb.AddForce(jump * jumpForce, ForceMode.Impulse);
        isGrounded = false;
    }
}
